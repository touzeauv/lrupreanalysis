
CMAKE_MINIMUM_REQUIRED(VERSION 3.1)

# configuration
set(PLUGIN	"lrupreanalysis")		# plugin name
set(NAMESPACE	"lrupreanalysis")		# namespace
set(SOURCES	"src/lrupreanalysis.cpp"
		"src/lrupreanalysis_CatBuilder.cpp"
		"src/lrupreanalysis_DUCatBuilder.cpp"
		"src/May/lrupreanalysis_MayAdapter.cpp"
		"src/May/lrupreanalysis_MayAnalysis.cpp"
		"src/May/lrupreanalysis_MayDomain.cpp"
		"src/Must/lrupreanalysis_MustAdapter.cpp"
		"src/Must/lrupreanalysis_MustAnalysis.cpp"
		"src/Must/lrupreanalysis_MustDomain.cpp"
		"src/EM/lrupreanalysis_ExistMissDomain.cpp"
		"src/EM/lrupreanalysis_ExistMissAdapter.cpp"
		"src/EM/lrupreanalysis_ExistMissAnalysis.cpp"
		"src/EH/lrupreanalysis_ExistHitDomain.cpp"
		"src/EH/lrupreanalysis_ExistHitAdapter.cpp"
		"src/EH/lrupreanalysis_ExistHitAnalysis.cpp"
)

# script
project(${PLUGIN})

# look for OTAWA
if(NOT OTAWA_CONFIG)
    find_program(OTAWA_CONFIG otawa-config DOC "path to otawa-config")
    if(NOT OTAWA_CONFIG)
        message(FATAL_ERROR "ERROR: otawa-config is required !")
    endif()
endif()

# building doc
find_package(Doxygen)
if (DOXYGEN_FOUND)
	# set input and output files
	set(DOXYGEN_IN ${CMAKE_CURRENT_SOURCE_DIR}/Doxyfile)
	set(DOXYGEN_OUT ${CMAKE_CURRENT_BINARY_DIR}/Doxyfile)

	# request to configure the file
	configure_file(${DOXYGEN_IN} ${DOXYGEN_OUT} @ONLY)

	add_custom_target(doc
        COMMAND ${DOXYGEN_EXECUTABLE} ${DOXYGEN_OUT}
        WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
        COMMENT "Generating documentation"
        VERBATIM)
else (DOXYGEN_FOUND)
  	message("Doxygen is needed to build the documentation")
endif (DOXYGEN_FOUND)

file(COPY ${CMAKE_CURRENT_SOURCE_DIR}/${PLUGIN}.eld DESTINATION ${CMAKE_CURRENT_BINARY_DIR}/)

message(STATUS "otawa-config found at ${OTAWA_CONFIG}")
execute_process(COMMAND "${OTAWA_CONFIG}" --cflags
		OUTPUT_VARIABLE OTAWA_CFLAGS
		WORKING_DIRECTORY "${CMAKE_CURRENT_BINARY_DIR}"
		OUTPUT_STRIP_TRAILING_WHITESPACE)
execute_process(COMMAND "${OTAWA_CONFIG}" --libs --make-plug ${PLUGIN} -r
		OUTPUT_VARIABLE OTAWA_LDFLAGS
		WORKING_DIRECTORY "${CMAKE_CURRENT_BINARY_DIR}"
		OUTPUT_STRIP_TRAILING_WHITESPACE)
execute_process(COMMAND "${OTAWA_CONFIG}" --prefix
		OUTPUT_VARIABLE OTAWA_PREFIX
		WORKING_DIRECTORY "${CMAKE_CURRENT_BINARY_DIR}"
		OUTPUT_STRIP_TRAILING_WHITESPACE)

# plugin definition
set(ORIGIN $ORIGIN)
add_library(${PLUGIN} SHARED ${SOURCES})
set_property(TARGET ${PLUGIN} PROPERTY PREFIX "")
set_property(TARGET ${PLUGIN} PROPERTY COMPILE_FLAGS "${OTAWA_CFLAGS} --std=c++11 -g -Wall -Wextra -pedantic")
target_include_directories(${PLUGIN} PUBLIC "${CMAKE_SOURCE_DIR}/include")
target_link_libraries(${PLUGIN} "${OTAWA_LDFLAGS}")


# installation
set(PLUGIN_PATH "${OTAWA_PREFIX}/lib/otawa/")
install(TARGETS ${PLUGIN} LIBRARY DESTINATION ${PLUGIN_PATH})
install(DIRECTORY "${CMAKE_CURRENT_SOURCE_DIR}/include/lrupreanalysis" DESTINATION "${OTAWA_PREFIX}/include/")
install(FILES ${PLUGIN}.eld DESTINATION ${PLUGIN_PATH})
