#ifndef LRUPREANALYSIS_EXIST_MISS_DOMAIN_H_
#define LRUPREANALYSIS_EXIST_MISS_DOMAIN_H_

#include <otawa/icat3/features.h>
#include <otawa/icache/features.h>
#include <elm/io/Output.h>

#include <lrupreanalysis/features.h>

namespace lrupreanalysis
{

namespace eh_em
{

class ExistMissDomain {
public:
	using t = otawa::icat3::ACS;

	ExistMissDomain(const otawa::icat3::LBlockCollection& coll, int set, const t* init);
	inline const t& bot(void) const { return _bot; }
	inline const t& top(void) const { return _top; }
	inline const t& init(void) const { return _init; }
	inline void print(const t& a, otawa::io::Output& out) const { a.print(_set, _coll, out); }
	inline elm::io::Printable<t, ExistMissDomain> print(const t& a) const { return elm::io::p(a, *this); }
	inline bool contains(const t& a, int i) { return(a[i] != _A); }
	inline void copy(t& d, const t& s) { d.copy(s); }
	bool equals(const t& a, const t& b) const;
	void join(t& d, const t& s);
	void fetch(t& a, const otawa::icat3::LBlock *lb, may_must::ACSManager& mayManager);
	void update(const otawa::icache::Access& access, t& a, may_must::ACSManager& mayManager);

private:
	int _n;
	t _bot, _top;
	otawa::hard::Cache::set_t _set;
	const otawa::icat3::LBlockCollection& _coll;
	int _A;
	const t& _init;
	t _tmp;
};

} // namespace eh_em

} // namespace lrupreanalysis

#endif // LRUPREANALYSIS_EXIST_MISS_DOMAIN_H_
