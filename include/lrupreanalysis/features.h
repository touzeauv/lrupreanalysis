#ifndef LRUPREANALYSIS__FEATURES_H_
#define LRUPREANALYSIS__FEATURES_H_

#include <otawa/hard/Cache.h>
#include <otawa/icache/features.h>
#include <otawa/icat3/features.h>
#include <otawa/proc/Feature.h>

//#include <lrupreanalysis/ExistHitDomain.h>
//#include <lrupreanalysis/ExistMissDomain.h>

namespace lrupreanalysis
{

namespace may_must
{

class CacheState;

class ACSManager
{
public:
	ACSManager(otawa::WorkSpace *ws);
	~ACSManager(void);
	void start(otawa::Block *b);
	void update(const otawa::icache::Access& acc);
	int mustAge(const otawa::icat3::LBlock *lb);
	int mayAge(const otawa::icat3::LBlock *lb);
	void print(const otawa::icat3::LBlock *lb, elm::io::Output& out = elm::cout);

private:
	CacheState& use(int set);
	const otawa::icat3::LBlockCollection& coll;
	elm::Vector<int> used;
	otawa::Block *_b;
	CacheState **_states;
};

extern otawa::p::feature MAY_ANALYSIS_FEATURE;
extern otawa::p::feature MUST_ANALYSIS_FEATURE;
extern otawa::p::feature MAY_MUST_CATEGORY_FEATURE;

} // namespace may_must

namespace eh_em
{

class CacheState;
class ExistHitDomain;
class ExistMissDomain;


class ACSManager
{
public:
	ACSManager(otawa::WorkSpace* ws);
	~ACSManager(void);
	void start(otawa::Block* b);
	void update(const otawa::icache::Access& acc);
	int mustAge(const otawa::icat3::LBlock* b);
	int mayAge(const otawa::icat3::LBlock* b);
	int existHitAge(const otawa::icat3::LBlock* b);
	int existMissAge(const otawa::icat3::LBlock* b);
	ExistHitDomain& ehDomain(int set);
	otawa::icat3::ACS* ehValue(int set);
	ExistMissDomain& emDomain(int set);
	otawa::icat3::ACS* emValue(int set);
	void print(const otawa::icat3::LBlock* b, otawa::io::Output& out);
private:
	CacheState& use(int set);

	lrupreanalysis::may_must::ACSManager _underlyingManager;
	const otawa::icat3::LBlockCollection& _coll;
	elm::Vector<int> _used;
	otawa::Block* _b;
	CacheState** _states;

	bool _hasExistHit;
	bool _hasExistMiss;
};

// Exist-Hit
extern otawa::p::feature EXIST_HIT_ANALYSIS_FEATURE;
extern otawa::p::id<otawa::icat3::Container<otawa::icat3::ACS> > EXIST_HIT_INIT;
extern otawa::p::id<otawa::icat3::Container<otawa::icat3::ACS> > EXIST_HIT_IN;

// Exist-Miss
extern otawa::p::feature EXIST_MISS_ANALYSIS_FEATURE;
extern otawa::p::id<otawa::icat3::Container<otawa::icat3::ACS> > EXIST_MISS_INIT;
extern otawa::p::id<otawa::icat3::Container<otawa::icat3::ACS> > EXIST_MISS_IN;

// Definitely Unknown

enum class DUCategory
{
	NC,
	EH,
	EM,
	DU
};

extern otawa::p::feature DU_CATEGORY_FEATURE;
extern otawa::p::id<DUCategory> DU_CATEGORY;

} // namespace eh_hm

} // namespace lrupreanalysis

#endif /* LRUPREANALYSIS_FEATURES_H_ */
