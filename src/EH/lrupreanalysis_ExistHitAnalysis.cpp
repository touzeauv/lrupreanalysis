#include "ExistHitAnalysis.h"

#include <chrono>

#include <otawa/ai/ArrayStore.h>
#include <otawa/ai/SimpleAI.h>
#include <otawa/icache/features.h>
#include <otawa/icat3/features.h>

#include <lrupreanalysis/features.h>

#include "ExistHitAdapter.h"
#include <lrupreanalysis/ExistHitDomain.h>

using namespace otawa;

namespace lrupreanalysis
{

namespace eh_em
{

ExistHitAnalysis::ExistHitAnalysis(p::declare& r) :
		Processor(r),
		_initExistHit(nullptr),
		_coll(nullptr),
		_cfgs(nullptr)
{
}

void ExistHitAnalysis::configure(const PropList& props)
{
	Processor::configure(props);
	if(props.hasProp(EXIST_HIT_INIT))
		_initExistHit = &EXIST_HIT_INIT(props);
}

void ExistHitAnalysis::setup(WorkSpace* ws)
{
	_coll = icat3::LBLOCKS(ws);
	ASSERT(_coll != nullptr);
	_cfgs = INVOLVED_CFGS(ws);
	ASSERT(_cfgs != nullptr);
}

void ExistHitAnalysis::processWorkSpace(WorkSpace* ws)
{
	auto start = std::chrono::system_clock::now();

	// prepare containers
	for(CFGCollection::BlockIter b(_cfgs); b(); b++)
		(*EXIST_HIT_IN(*b)).configure(*_coll);

	// compute ACS
	for(int i = 0; i < _coll->cache()->setCount(); i++) {
		if((*_coll)[i].count()) {
			if(logFor(LOG_FUN))
				log << "\tanalyzing set " << i << io::endl;
			processSet(i, ws);
		}
	}

	auto end = std::chrono::system_clock::now();
	auto elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(end - start);
	if(logFor(LOG_FUN))
		log << "\tExist Hit Analysis running time: " << elapsed.count() << " ms" << io::endl;
}

void ExistHitAnalysis::destroy(WorkSpace*)
{
	for(CFGCollection::BlockIter b(_cfgs); b(); b++)
		EXIST_HIT_IN(*b).remove();
}

void ExistHitAnalysis::processSet(int set, WorkSpace* ws)
{
	// perform the analysis
	ExistHitAdapter ada(set, _initExistHit ? &_initExistHit->get(set) : nullptr, *_coll, *_cfgs, ws);
	ai::SimpleAI<ExistHitAdapter> ana(ada);
	ana.run();

	// store the results
	for(CFGCollection::BlockIter b(_cfgs); b(); b++)
		if(b->isBasic() || b->isEntry()) {
			ada.domain().copy((*EXIST_HIT_IN(*b))[set], ada.store().get(*b));
//			if(logFor(LOG_BLOCK))
//				log << "\t\t\t" << *b << ": " << ada.domain().print(ada.store().get(b)) << io::endl;
		}
}

p::declare ExistHitAnalysis::reg = p::init("lrupreanalysis::eh_em::ExistHitAnalysis", Version(1, 0, 0))
	.require(icat3::LBLOCKS_FEATURE)
	.require(COLLECTED_CFG_FEATURE)
	.require(icat3::MUST_PERS_ANALYSIS_FEATURE)
	.require(icat3::CATEGORY_FEATURE)
	.provide(EXIST_HIT_ANALYSIS_FEATURE)
	.make<ExistHitAnalysis>();

/**
 * Perform a Exist-Hit cache analysis.
 * It computes a safe upper bound on blocks age that is valid for at least one execution paths (assuming they are all feasible).
 * @par Properties
 * @li @ref otawa::lrupreanalysis::eh_em::EXIST_HIT_IN
 *
 * @par configuration
 * @li @ref otawa::lrupreanalysis::eh_em::EXIST_HIT_INIT
 *
 * @par Implementation
 * @li @ref lrupreanalysis::eh_em::ExistHitAnalysis
 *
 * @ingroup lrupreanalysis
 */
p::feature EXIST_HIT_ANALYSIS_FEATURE("lrupreanalysis::eh_em::EXIST_HIT_ANALYSIS_FEATURE", p::make<ExistHitAnalysis>());

/**
 * Abstract Cache State computed by the Exist-Hit analysis at the entry of the corresponding block or edge.
 *
 * @par Feature
 * @li @ref EXIST_HIT_ANALYSIS_FEATURE
 *
 * @par Hooks
 * @li @ref Block
 *
 * @ingroup lrupreanalysis
 */
p::id<icat3::Container<icat3::ACS> > EXIST_HIT_IN("lrupreanalysis::eh_em::EXIST_HIT_IN");

/**
 * Initial state for Exist-Hit instruction cache analysis.
 *
 * @par Hook
 * @li Feature configuration.
 *
 * @par Feature
 * @li @ref EXIST_HIT_ANALYSIS_FEATURE
 *
 * @ingroup lrupreanalysis
 */
p::id<icat3::Container<icat3::ACS> > EXIST_HIT_INIT("lrupreanalysis::eh_em::EXIST_HIT_INIT");

} // namespace eh_em

} // namespace lrupreanalysis

