#include <lrupreanalysis/ExistHitDomain.h>

using namespace otawa;

namespace lrupreanalysis
{

namespace eh_em
{

ExistHitDomain::ExistHitDomain(
		const icat3::LBlockCollection& coll,
		int set,
		const t *init) :
	_n(coll[set].count()),
	_bot(_n, icat3::BOT_AGE),
	_top(_n, 0),
	_set(set),
	_coll(coll),
	_A(coll.A()),
	_init(init ? *init : _top),
	_tmp(_n)
{
}

bool ExistHitDomain::equals(const t& a, const t& b) const
{
	for(int i = 0; i < _n; i++)
		if(a[i] != b[i])
			return false;

	return true;
}
void ExistHitDomain::join(t& d, const t& s)
{
	for(int i = 0; i < _n; i++) {
		// d[i] = min(d[i], s[i])
		if(d[i] == icat3::BOT_AGE)
			d[i] = s[i];
		else if(s[i] != icat3::BOT_AGE)
			d[i] = min(d[i], s[i]);

	}
}

void ExistHitDomain::fetch(t& a, const icat3::LBlock *lb, may_must::ACSManager& mustManager)
{
	int b = lb->index();

	if(a[0] == icat3::BOT_AGE)
		return;

	for(int i = 0; i < _n; i++) {
		ASSERT(a[i] != icat3::BOT_AGE);
		if(i == b)
			continue;

		if(a[i] < mustManager.mustAge(lb) && a[i] < _A)
			a[i]++;
	}
	a[b] = 0;
}

void ExistHitDomain::update(const icache::Access& access, t& a, may_must::ACSManager& mustManager)
{
	switch(access.kind()) {

	case icache::FETCH:
		if(_coll.cache()->set(access.address()) == _set)
			fetch(a, icat3::LBLOCK(access), mustManager);
		break;

	case icache::PREFETCH:
		if(_coll.cache()->set(access.address()) == _set) {
			copy(_tmp, a);
			fetch(a, icat3::LBLOCK(access), mustManager);
			join(a, _tmp);
		}
		break;

	case icache::NONE:
		break;

	default:
		ASSERT(false);
	}
}

} // namespace eh_em

} // namespace lrupreanalysis
