#ifndef LRUPREANALYSIS_EXIST_MISS_ADAPTER_H_
#define LRUPREANALYSIS_EXIST_MISS_ADAPTER_H_

#include <otawa/ai/ArrayStore.h>
#include <otawa/icat3/features.h>
#include <otawa/cfg/CompositeCFG.h>
#include <otawa/cfg/features.h>

#include <lrupreanalysis/features.h>

#include <lrupreanalysis/ExistMissDomain.h>

namespace lrupreanalysis
{

namespace eh_em
{

class ExistMissAdapter
{
public:
	using domain_t = ExistMissDomain;
	using t = typename domain_t::t;
	using graph_t = otawa::CompositeCFG;
	using store_t = otawa::ai::ArrayStore<domain_t, graph_t>;

	ExistMissAdapter(int set, const t* init, const otawa::icat3::LBlockCollection& coll, const otawa::CFGCollection& cfgs, otawa::WorkSpace* ws);

	inline domain_t& domain(void) { return _domain; }
	inline graph_t& graph(void) { return _graph; }
	inline store_t& store(void) { return _store; }

	void update(const otawa::Bag<otawa::icache::Access>& accs, t& d);
	void update(otawa::Block *v, t& d);

private:
	may_must::ACSManager _mayManager;
	domain_t _domain;
	graph_t _graph;
	store_t _store;
};

} // namespace eh_em

} // namespace lrupreanalysis

#endif // LRUPREANALYSIS_EXIST_MISS_ADAPTER_H_
