#ifndef LRUPREANALYSIS_EXIST_MISS_ANALYSIS_H_
#define LRUPREANALYSIS_EXIST_MISS_ANALYSIS_H_

#include <otawa/proc/Processor.h>
#include <otawa/icat3/features.h>
#include <otawa/cfg/features.h>

namespace lrupreanalysis
{

namespace eh_em
{

class ExistMissAnalysis: public otawa::Processor
{
public:
	static otawa::p::declare reg;
	ExistMissAnalysis(otawa::p::declare& r = reg);

protected:

	void configure(const otawa::PropList& props) override;
	void setup(otawa::WorkSpace* ws) override;
	void processWorkSpace(otawa::WorkSpace* ws) override;
	void destroy(otawa::WorkSpace* ws) override;

private:
	void processSet(int i, otawa::WorkSpace* ws);

	const otawa::icat3::Container<otawa::icat3::ACS>* _initExistMiss;
	const otawa::icat3::LBlockCollection* _coll;
	const otawa::CFGCollection* _cfgs;
};

/**
 * Perform the ACS analysis for the Exist-Miss domain. For cache block, it associates a lower bound on block age that holds on at least on path
 * @par Properties
 * @li @ref EXIST_MISS_IN
 *
 * @par Configuraiton
 * @li @ref EXIST_MISS_INIT
 *
 * @par Implementation
 * @li @ref ExistMissAnalysis
 *
 * @ingroup lrupreanalysis
 */
extern otawa::p::feature EXIST_MISS_ANALYSIS_FEATURE;

/**
 * ACS for the Exist-Miss analysis at the entry of the corresponding block or edge.
 *
 * @par Feature
 * @li @ref EXIST_MISS_ANALYSIS_FEATURE
 *
 * @par Hooks
 * @li @ref Block
 * @li @ref Edge
 *
 * @ingroup lrupreanalysis
 */
extern otawa::p::id<otawa::icat3::Container<otawa::icat3::ACS> > EXIST_MISS_IN;


/**
 * Initial state for Exist-Miss instruction cache analysis.
 *
 * @par Hook
 * @li Feature configuration.
 *
 * @par Feature
 * @li @ref EXIST_MISS_ANALYSIS_FEATURE
 *
 * @ingroup lrupreanalysis
 */
extern otawa::p::id<otawa::icat3::Container<otawa::icat3::ACS> > EXIST_MISS_INIT;

} // namespace eh_em

} // namespace lrupreanalysis

#endif // LRUPREANALYSIS_EXIST_MISS_ANALYSIS_H_

