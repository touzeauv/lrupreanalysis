#include <lrupreanalysis/ExistMissDomain.h>

using namespace otawa;

namespace lrupreanalysis
{

namespace eh_em
{

ExistMissDomain::ExistMissDomain(
		const icat3::LBlockCollection& coll,
		int set,
		const t *init) :
	_n(coll[set].count()),
	_bot(_n, icat3::BOT_AGE),
	_top(_n, coll.A()),
	_set(set),
	_coll(coll),
	_A(coll.A()),
	_init(init ? *init : _top),
	_tmp(_n)
{
}

bool ExistMissDomain::equals(const t& a, const t& b) const
{
	for(int i = 0; i < _n; i++)
		if(a[i] != b[i])
			return false;

	return true;
}
void ExistMissDomain::join(t& d, const t& s)
{
	for(int i = 0; i < _n; i++)
		d[i] = max(d[i], s[i]);
}

void ExistMissDomain::fetch(t& a, const icat3::LBlock *lb, may_must::ACSManager& mayManager)
{
	int b = lb->index();

	if(a[0] == icat3::BOT_AGE)
		return;

	for(int i = 0; i < _n; i++) {
		if(i == b)
			continue;

		if(a[i] <= mayManager.mayAge(lb) && a[i] != _A)
			a[i]++;
	}
	a[b] = 0;
}

void ExistMissDomain::update(const icache::Access& access, t& a, may_must::ACSManager& mayManager)
{
	switch(access.kind()) {

	case icache::FETCH:
		if(_coll.cache()->set(access.address()) == _set)
			fetch(a, icat3::LBLOCK(access), mayManager);
		break;

	case icache::PREFETCH:
		if(_coll.cache()->set(access.address()) == _set) {
			copy(_tmp, a);
			fetch(a, icat3::LBLOCK(access), mayManager);
			join(a, _tmp);
		}
		break;

	case icache::NONE:
		break;

	default:
		ASSERT(false);
	}
}

} // namespace eh_em

} // namespace lrupreanalysis
