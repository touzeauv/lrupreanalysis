#ifndef OTAWALRU_MAY_ADAPTER_H_
#define OTAWALRU_MAY_ADAPTER_H_

#include <otawa/ai/ArrayStore.h>
#include <otawa/icat3/features.h>
#include <otawa/cfg/CompositeCFG.h>
#include <otawa/cfg/features.h>

#include "MayDomain.h"

namespace lrupreanalysis
{

class MayAdapter
{
public:
	using domain_t = MayDomain;
	using t = typename domain_t::t;
	using graph_t = otawa::CompositeCFG;
	using store_t = otawa::ai::ArrayStore<domain_t, graph_t>;

	MayAdapter(int set, const t* init, const otawa::icat3::LBlockCollection& coll, const otawa::CFGCollection& cfgs, otawa::WorkSpace* ws);

	inline domain_t& domain(void) { return _domain; }
	inline graph_t& graph(void) { return _graph; }
	inline store_t& store(void) { return _store; }

	void update(const otawa::Bag<otawa::icache::Access>& accs, t& d);
	void update(otawa::Block *v, t& d);

private:
	domain_t _domain;
	graph_t _graph;
	store_t _store;
};

} // namespace lrupreanalysis

#endif // OTAWALRU_MAY_ADAPTER_H_
