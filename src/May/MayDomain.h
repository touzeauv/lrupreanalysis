#ifndef OTAWALRU_MAY_DOMAIN_H_
#define OTAWALRU_MAY_DOMAIN_H_

#include <otawa/icat3/features.h>
#include <otawa/icache/features.h>

namespace lrupreanalysis
{

class MayDomain {
public:
	using t = otawa::icat3::ACS;

	MayDomain(const otawa::icat3::LBlockCollection& coll, int set, const t* init);
	inline const t& bot(void) const { return _bot; }
	inline const t& top(void) const { return _top; }
	inline const t& init(void) const { return _init; }
	inline void print(const t& a, otawa::io::Output& out) const { a.print(_set, _coll, out); }
	inline bool contains(const t& a, int i) { return(a[i] != _A); }
	inline void copy(t& d, const t& s) { d.copy(s); }
	bool equals(const t& a, const t& b) const;
	void join(t& d, const t& s);
	void fetch(t& a, const otawa::icat3::LBlock *lb);
	void update(const otawa::icache::Access& access, t& a);

private:
	int _n;
	t _bot, _top;
	otawa::hard::Cache::set_t _set;
	const otawa::icat3::LBlockCollection& _coll;
	int _A;
	const t& _init;
	t _tmp;
};

} // namespace lrupreanalysis

#endif // OTAWALRU_MAY_DOMAIN_H_
