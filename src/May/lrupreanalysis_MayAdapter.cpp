#include "MayAdapter.h"

using namespace otawa;

namespace lrupreanalysis
{

MayAdapter::MayAdapter(
		int set,
		const t* init,
		const icat3::LBlockCollection& coll,
		const CFGCollection& cfgs,
		otawa::WorkSpace*) :
	_domain(coll, set, init),
	_graph(cfgs),
	_store(_domain, _graph)
{
}

void MayAdapter::update(const Bag<icache::Access>& accs, t& d)
{
	for(int i = 0; i < accs.count(); i++) {
		_domain.update(accs[i], d);
	}
}

void MayAdapter::update(Block *v, t& d)
{
	_domain.copy(d, _domain.bot());
	t s;

	// update and join along edges
	for(auto e = _graph.preds(v); e(); e++) {
		Block *w = e->source();
		_domain.copy(s, _store.get(w));

		// apply block
		{
			const Bag<icache::Access>& accs = icache::ACCESSES(w);
			if(accs.count() > 0) {
				update(accs, s);
			}
		}

		// apply edge
		{
			const Bag<icache::Access>& accs = icache::ACCESSES(*e);
			if(accs.count() > 0) {
				update(accs, s);
			}
		}


		// merge result
		_domain.join(d, s);
	}
}

} // namespace lrupreanalysis

