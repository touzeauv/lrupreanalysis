#include "MayAnalysis.h"

#include <chrono>
#include <otawa/ai/ArrayStore.h>
#include <otawa/ai/SimpleAI.h>
#include <otawa/icache/features.h>
#include <otawa/icat3/features.h>

#include <lrupreanalysis/features.h>

#include "MayAdapter.h"
#include "MayDomain.h"

using namespace otawa;

namespace lrupreanalysis
{

namespace may_must
{

MayAnalysis::MayAnalysis(p::declare& r) :
		Processor(r),
		_initMay(nullptr),
		_coll(nullptr),
		_cfgs(nullptr)
{
}

void MayAnalysis::configure(const PropList& props)
{
	Processor::configure(props);
	if(props.hasProp(icat3::MAY_INIT))
		_initMay = &icat3::MAY_INIT(props);
}

void MayAnalysis::setup(WorkSpace* ws)
{
	_coll = icat3::LBLOCKS(ws);
	ASSERT(_coll != nullptr);
	_cfgs = INVOLVED_CFGS(ws);
	ASSERT(_cfgs != nullptr);
}

void MayAnalysis::processWorkSpace(WorkSpace* ws)
{
	auto start = std::chrono::system_clock::now();

	// prepare containers
	for(CFGCollection::BlockIter b(_cfgs); b(); b++)
		(*icat3::MAY_IN(*b)).configure(*_coll);

	// compute ACS
	for(int i = 0; i < _coll->cache()->setCount(); i++) {
		if((*_coll)[i].count()) {
			if(logFor(LOG_FUN))
				log << "\tanalyzing set " << i << io::endl;
			processSet(i, ws);
		}
	}

	auto end = std::chrono::system_clock::now();
	auto elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(end - start);
	if(logFor(LOG_FUN))
		log << "\tMay Analysis running time: " << elapsed.count() << " ms" << io::endl;
}

void MayAnalysis::destroy(WorkSpace*)
{
	for(CFGCollection::BlockIter b(_cfgs); b(); b++)
		icat3::MAY_IN(*b).remove();
}

void MayAnalysis::processSet(int set, WorkSpace* ws)
{
	// perform the analysis
	MayAdapter ada(set, _initMay ? &_initMay->get(set) : nullptr, *_coll, *_cfgs, ws);
	ai::SimpleAI<MayAdapter> ana(ada);
	ana.run();

	// store the results
	for(CFGCollection::BlockIter b(_cfgs); b(); b++)
		if(b->isBasic()) {
			ada.domain().copy((*icat3::MAY_IN(*b))[set], ada.store().get(*b));
//			if(logFor(LOG_BLOCK)) {
//				log << "\t\t\t" << *b << ": ";
//				ada.domain().print(ada.store().get(b), log);
//				log << io::endl;
//			}
		}
}

p::declare MayAnalysis::reg = p::init("lrupreanalysis::may_must::MayAnalysis", Version(1, 0, 0))
	.require(icat3::LBLOCKS_FEATURE)
	.require(COLLECTED_CFG_FEATURE)
	.provide(MAY_ANALYSIS_FEATURE)
	.provide(icat3::MAY_ANALYSIS_FEATURE)
	.make<MayAnalysis>();

/**
 * Perform a May cache analysis.
 * It computes a safe lower bound on blocks age that is valid for all execution paths.
 * @par Properties
 * @li @ref otawa::icat3::MAY_IN
 *
 * @par configuration
 * @li @ref otawa::icat3::MAY_INIT
 *
 * @par Implementation
 * @li @ref lrupreanalysis::may_must::MayAnalysis
 *
 * @ingroup lrupreanalysis
 */
p::feature MAY_ANALYSIS_FEATURE("lrupreanalysis::may_must::MAY_ANALYSIS_FEATURE", p::make<MayAnalysis>());

} // namespace may_must

} // namespace lrupreanalysis
