#include "MayDomain.h"

using namespace otawa;

namespace lrupreanalysis
{

MayDomain::MayDomain(
		const icat3::LBlockCollection& coll,
		int set,
		const t *init) :
	_n(coll[set].count()),
	_bot(_n, icat3::BOT_AGE),
	_top(_n, 0),
	_set(set),
	_coll(coll),
	_A(coll.A()),
	_init(init ? *init : _top),
	_tmp(_n)
{
}

bool MayDomain::equals(const t& a, const t& b) const
{
	for(int i = 0; i < _n; i++) {
		if(a[i] != b[i]) {
			return false;
		}
	}

	return true;
}
void MayDomain::join(t& d, const t& s)
{
	for(int i = 0; i < _n; i++) {
		//d[i] = min(d[i], s[i]);
		if(d[i] == icat3::BOT_AGE) {
			d[i] = s[i];
		}
		else if(s[i] != icat3::BOT_AGE) {
			d[i] = min(d[i], s[i]);
		}
	}
}

void MayDomain::fetch(t& a, const icat3::LBlock *lb)
{
	int b = lb->index();

	if(a[0] == icat3::BOT_AGE) {
		return;
	}

	for(int i = 0; i < _n; i++) {
		if(i == b) {
			continue;
		}

		if(a[i] <= a[b] && a[i] != _A) {
			a[i]++;
		}
	}
	a[b] = 0;
}

void MayDomain::update(const icache::Access& access, t& a)
{
	switch(access.kind()) {

	case icache::FETCH:
		if(_coll.cache()->set(access.address()) == _set) {
			fetch(a, icat3::LBLOCK(access));
		}
		break;

	case icache::PREFETCH:
		if(_coll.cache()->set(access.address()) == _set) {
			copy(_tmp, a);
			fetch(a, icat3::LBLOCK(access));
			join(a, _tmp);
		}
		break;

	case icache::NONE:
		break;

	default:
		ASSERT(false);
	}
}

} // namespace lrupreanalysis

