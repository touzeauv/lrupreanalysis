#ifndef OTAWALRU_MUST_ANALYSIS_H_
#define OTAWALRU_MUST_ANALYSIS_H_

#include <otawa/proc/Processor.h>
#include <otawa/icat3/features.h>
#include <otawa/cfg/features.h>

namespace lrupreanalysis
{

namespace may_must
{

class MustAnalysis: public otawa::Processor
{
public:
	static otawa::p::declare reg;
	MustAnalysis(otawa::p::declare& r = reg);

protected:

	void configure(const otawa::PropList& props) override;
	void setup(otawa::WorkSpace* ws) override;
	void processWorkSpace(otawa::WorkSpace* ws) override;
	void destroy(otawa::WorkSpace* ws) override;

private:
	void processSet(int set, otawa::WorkSpace* ws);

	const otawa::icat3::Container<otawa::icat3::ACS>* _initMust;
	const otawa::icat3::LBlockCollection* _coll;
	const otawa::CFGCollection* _cfgs;
};

} // namespace may_must

} // namespace lrupreanalysis

#endif // OTAWALRU_MUST_ANALYSIS_H_
