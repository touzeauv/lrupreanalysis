#include "MustAnalysis.h"

#include <chrono>
#include <otawa/ai/ArrayStore.h>
#include <otawa/ai/SimpleAI.h>
#include <otawa/icache/features.h>
#include <otawa/icat3/features.h>

#include <lrupreanalysis/features.h>

#include "MustAdapter.h"
#include "MustDomain.h"

using namespace otawa;

namespace lrupreanalysis
{

namespace may_must
{

MustAnalysis::MustAnalysis(p::declare& r) :
		Processor(r),
		_initMust(nullptr),
		_coll(nullptr),
		_cfgs(nullptr)
{
}

void MustAnalysis::configure(const PropList& props)
{
	Processor::configure(props);
	if(props.hasProp(icat3::MUST_INIT)) {
		_initMust = &icat3::MUST_INIT(props);
	}
}

void MustAnalysis::setup(WorkSpace* ws)
{
	_coll = icat3::LBLOCKS(ws);
	ASSERT(_coll != nullptr);
	_cfgs = INVOLVED_CFGS(ws);
	ASSERT(_cfgs != nullptr);
}

void MustAnalysis::processWorkSpace(WorkSpace* ws)
{
	auto start = std::chrono::system_clock::now();

	// prepare containers
	for(CFGCollection::BlockIter b(_cfgs); b(); b++) {
		(*icat3::MUST_IN(*b)).configure(*_coll);
	}

	// compute ACS
	for(int i = 0; i < _coll->cache()->setCount(); i++) {
		if((*_coll)[i].count() != 0) {
			if(logFor(LOG_FUN)) {
				log << "\tanalyzing set " << i << io::endl;
			}
			processSet(i, ws);
		}
	}

	auto end = std::chrono::system_clock::now();
	auto elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(end - start);
	if(logFor(LOG_FUN)) {
		log << "\tMust Analysis running time: " << elapsed.count() << " ms" << io::endl;
	}
}

void MustAnalysis::destroy(WorkSpace*)
{
	for(CFGCollection::BlockIter b(_cfgs); b(); b++) {
		icat3::MUST_IN(*b).remove();
	}
}

void MustAnalysis::processSet(int set, WorkSpace* ws)
{
	// perform the analysis
	MustAdapter ada(set, _initMust ? &_initMust->get(set) : nullptr, *_coll, *_cfgs, ws);
	ai::SimpleAI<MustAdapter> ana(ada);
	ana.run();

	// store the results
	for(CFGCollection::BlockIter b(_cfgs); b(); b++) {
		if(b->isBasic()) {
			ada.domain().copy((*icat3::MUST_IN(*b))[set], ada.store().get(*b));
//			if(logFor(LOG_BLOCK)) {
//				log << "\t\t\t" << *b << ": ";
//				ada.domain().print(ada.store().get(b), log);
//				log << io::endl;
//			}
		}
	}
}

p::declare MustAnalysis::reg = p::init("lrupreanalysis::may_must::MustAnalysis", Version(1, 0, 0))
	.require(icat3::LBLOCKS_FEATURE)
	.require(COLLECTED_CFG_FEATURE)
	.provide(MUST_ANALYSIS_FEATURE)
	.provide(icat3::MUST_PERS_ANALYSIS_FEATURE)
	.make<MustAnalysis>();

/**
 * Perform a Must cache analysis.
 * It computes a safe upper bound on blocks age that is valid for all execution paths.
 * @par Properties
 * @li @ref otawa::icat3::MUST_IN
 *
 * @par configuration
 * @li @ref otawa::icat3::MUST_INIT
 *
 * @par Implementation
 * @li @ref lrupreanalysis::may_must::MustAnalysis
 *
 * @ingroup lrupreanalysis
 */
p::feature MUST_ANALYSIS_FEATURE("lrupreanalysis::may_must::MUST_ANALYSIS_FEATURE", p::make<MustAnalysis>());

} // namespace may_must

} // namespace lrupreanalysis

