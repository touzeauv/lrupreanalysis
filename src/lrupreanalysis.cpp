#include <otawa/proc/ProcessorPlugin.h>


namespace lrupreanalysis
{

/**
 * @defgroup lrupreanalysis	LRU Cache Analysis Module
 *
 * @mainpage
 *
 * lrupreanalysis is a plugin of Otawa that provides several analyses of LRU L1 instruction caches performed by abstract interpretation.
 * It aims at speeding-up complex (and more costly) precise analyses.
 * Mainly, it provides this module following analyses:
 * @li Must analysis: this is the usual Must Analysis described in the paper "Cache Behavior Prediction by Abstract Interpretation" (SAS96). It is used to classify some memory accesses as "Always Hit".
 * @li May analysis: this is the dual of the Must analysis, that is used to predict misses.
 * @li Exist-Hit analysis: this analysis is described in the paper "Ascertaining Uncertainty for Efficient Exact Cache Analysis" (CAV17). It classifies a memory access as "Exist-Hit" if it can find at least one execution path that makes the given access a hit.
 * @li Exist-Miss analysis: this analysis is the dual of the last one. It is used to guarantee that some memory accesses are misses for at least one execution path.
 */

using namespace elm;
using namespace otawa;

class Plugin: public ProcessorPlugin {
public:
	Plugin(): ProcessorPlugin("lrupreanalysis", Version(1, 0, 0), OTAWA_PROC_VERSION) { }
};


} // namespace lrupreanalysis

lrupreanalysis::Plugin lrupreanalysis_plugin;
ELM_PLUGIN(lrupreanalysis_plugin, OTAWA_PROC_HOOK)

