/*
 *	icat3::CatBuilder class implementation
 *	Copyright (c) 2017, IRIT UPS.
 *
 *	This file is part of OTAWA
 *
 *	OTAWA is free software; you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation; either version 2 of the License, or
 *	(at your option) any later version.
 *
 *	OTAWA is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with OTAWA; if not, write to the Free Software
 *	Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 *	02110-1301  USA
 */

#include <lrupreanalysis/features.h>

#include <otawa/cfg.h>
#include <otawa/hard/Memory.h>
#include <otawa/icat3/features.h>
#include <otawa/proc/BBProcessor.h>
#include <otawa/program.h>

#include "Must/MustDomain.h"
#include "May/MayDomain.h"

using namespace otawa;

namespace lrupreanalysis
{

namespace may_must
{

class CacheState
{
public:
	CacheState(const icat3::LBlockCollection& coll, int set) :
		must_dom(coll, set, nullptr),
		may_dom(coll, set, nullptr),
		must_state(nullptr),
		may_state(nullptr)
	{
	}

	~CacheState()
	{
		if(must_state != nullptr) {
			delete must_state;
		}
		if(may_state != nullptr) {
			delete may_state;
		}
	}

	MustDomain must_dom;
	MayDomain may_dom;
	typename MustDomain::t *must_state;
	typename MayDomain::t *may_state;
};

/**
 * @class ACSManager;
 * The ACS (Abstract Cache State) manager is analoguous to the icat3 ACS manager and provides a way to use the results of @ref MayAnalysis and @ref MustAnalysis.
 * The analyses computes ACSs at the entry of basic block.
 * The role of ACS manager is then to rebuild the ACS after some accesses (i.e. in the middle of a basic block) from the entry ACS.
 *
 * To use the manager, call the start method to initialize the ACS manager initial state with the ACS at the provided block entry.
 * Then, call update for each access met to update the internat cache state.
 * The information about the internal state can then be retrieve using the mayAge and mustAge.
 *
 * @par Used features
 * @li @ref lrupreanalysis::may_must::MUST_ANALYSIS_FEATURE (required)
 * @li @ref lrupreanalysis::may_must::MAY_ANALYSIS_FEATURE (required)
 *
 * @ingroup lrupreanalysis
 */


/**
 * Build the ACS manager.
 * @param ws	Workspace to work on.
 */
ACSManager::ACSManager(WorkSpace *ws): coll(**icat3::LBLOCKS(ws)), _b(0)
{
	_states = new CacheState *[coll.sets()];
	array::set(_states, coll.sets(), null<CacheState>());
}

/**
 */
ACSManager::~ACSManager()
{
	for(int i = 0; i < coll.sets(); i++) {
		if(_states[i] != nullptr) {
			delete _states[i];
		}
	}
	delete [] _states;
}

/**
 * Start using ACS information for block b.
 * @param b		Block to be explored.
 */
void ACSManager::start(Block *b)
{
	ASSERT(b);
	_b = b;
	used.clear();
}

/**
 * Update the ACS considering that the access is performed.
 * @param acc	Performed access.
 */
void ACSManager::update(const icache::Access& acc)
{
	icat3::LBlock *lb = icat3::LBLOCK(acc);
	CacheState& s = use(lb->set());
	s.must_dom.update(acc, *s.must_state);
	s.may_dom.update(acc, *s.may_state);
}

/**
 * Get the age of the given l-block for MUST analysis.
 * @param lb	Looked l-block.
 * @return		Age of lb.
 */
int ACSManager::mustAge(const icat3::LBlock* lb)
{
	return use(lb->set()).must_state->get(lb->index());
}

/**
 * Get the age of the given l-block for MAY analysis.
 * @param lb	Looked l-block.
 * @return		Age of lb.
 */
int ACSManager::mayAge(const icat3::LBlock* lb)
{
	return use(lb->set()).may_state->get(lb->index());
}

/**
 * Print the ACS corresponding to the given l-block.
 * @param lb	Looked l-block.
 * @param out	Output stream.
 */
void ACSManager::print(const icat3::LBlock *lb, Output& out)
{
	CacheState& state = use(lb->set());
	out << "{MUST: ";
	state.must_dom.print(*state.must_state, out);
	out << ", MAY: ";
	state.may_dom.print(*state.may_state, out);
	out << "}";
}

/**
 * Retrieve the cache state corresponding to the given set.
 * @param set	Considered set.
 * @return		Corresponding cache state.
 */
CacheState& ACSManager::use(int set)
{
	if(!used.contains(set)) {
		if(_states[set] == nullptr) {
			_states[set] = new CacheState(coll, set);
		}

		if(_states[set]->must_state == nullptr) {
			_states[set]->must_state = new MustDomain::t;
		}

		_states[set]->must_dom.copy(*_states[set]->must_state, (*icat3::MUST_IN(_b))[set]);

		if(_states[set]->may_state == nullptr) {
			_states[set]->may_state = new MayDomain::t;
		}
		_states[set]->may_dom.copy(*_states[set]->may_state, (*icat3::MAY_IN(_b))[set]);
		used.push(set);
	}
	return *_states[set];
}

/**
 * @class CatBuilder;
 *
 * The Cat builder processor is in charge of building the icat3::CATEGORY_FEATURE using the results of the MUST_ANALYSIS_FEATURE and MAY_ANALYSIS_FEATURE.
 *
 * @par Used features
 * @li @ref otawa::hard::MEMORY_FEATURE
 * @li @ref otawa::icat3::LBLOCKS_FEATURE
 * @li @ref lrupreanalysis::may_must::MUST_ANALYSIS_FEATURE (required)
 * @li @ref lrupreanalysis::may_must::MAY_ANALYSIS_FEATURE (required)
 *
 * @par Provided feature
 * @li @ref otawa::icat3::CATEGORY_FEATURE
 * @li @ref lrupreanalysis::CATEGORY_FEATURE
 *
 * @ingroup lrupreanalysis
 */
class CatBuilder: public BBProcessor
{
public:
	static p::declare reg;
	CatBuilder():
		BBProcessor(reg),
		coll(nullptr),
		A(0),
		man(nullptr)
	{
	}

protected:

	typedef icat3::ACS acs_t;

	/**
	 */
	virtual void setup(WorkSpace *ws) override
	{
		coll = icat3::LBLOCKS(ws);
		ASSERT(coll);
		A = coll->A();
		man = new may_must::ACSManager(ws);
	}

	/**
	 */
	virtual void cleanup(WorkSpace*) override
	{
		delete man;
	}

	/**
	 * Computes the category associated to the accesses associated to a basic block (accesses stored on the block itself, and on the incoming edges).
	 */
	virtual void processBB(WorkSpace*, CFG*, Block *v) override
	{
		for(Block::EdgeIter e = v->outs(); e(); e++) {
			if(logFor(LOG_BLOCK)) {
				log << "\t\t\t\tprocess " << *e << io::endl;
			}

			if(v->isSynth() && v->toSynth()->callee()) {
				man->start(v->toSynth()->callee()->exit());
			}
			else {
				man->start(v);
			}
			processAccesses(*icache::ACCESSES(v));
			processAccesses(*icache::ACCESSES(*e));
		}
	}

	/**
	 * Computes the category associated to the given accesses.
	 * This method is called by the processBB.
	 * @param accs	The accesses to classify.
	 */
	void processAccesses(Bag<icache::Access>& accs)
	{
		for(int i = 0; i < accs.count(); i++) {
			icat3::LBlock *lb = icat3::LBLOCK(accs[i]);
			icat3::age_t mustAge = man->mustAge(lb);
			icat3::age_t mayAge = man->mayAge(lb);
			icat3::category_t cat = icat3::NC;

			// in Must ACS => AH
			if(0 <= mustAge && mustAge < A) {
				cat = icat3::AH;
			}

			// not in May ACS => AM
			if(mayAge == A) {
				cat = icat3::AM;
			}

			// assign category
			icat3::CATEGORY(accs[i]) = cat;
			if(logFor(LOG_BLOCK)) {
				log << "\t\t\t\t" << accs[i] << ": " << icat3::CATEGORY(accs[i]);
//				log << " ACS = "; man->print(lb, log); log << io::endl;
			}

			// update the current ACS
			man->update(accs[i]);
		}
	}

	const icat3::LBlockCollection *coll;
	int A;
	may_must::ACSManager *man;
};

p::declare CatBuilder::reg = p::init("lrupreanalysis::may_must::CatBuilder", Version(1, 0, 0))
	.extend<BBProcessor>()
	.make<CatBuilder>()
	.require(icat3::LBLOCKS_FEATURE)
	.require(MAY_ANALYSIS_FEATURE)
	.require(MUST_ANALYSIS_FEATURE)
	.provide(MAY_MUST_CATEGORY_FEATURE)
	.provide(icat3::CATEGORY_FEATURE);

/**
 *
 * @par Properties
 * @li @ref icat3::CATEGORY
 *
 * @par Default implementation
 * @li @ref CatBuilder
 *
 * @ingroup lrupreanalysis
 */
p::feature MAY_MUST_CATEGORY_FEATURE("lrupreanalysis::may_must::MAY_MUST_CATEGORY_FEATURE", CatBuilder::reg);

} // namespace may_must

} // namespace lrupreanalysis

