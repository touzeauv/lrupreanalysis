#ifndef LRUPREANALYSIS_DU_CAT_BUILDER_H_
#define LRUPREANALYSIS_DU_CAT_BUILDER_H_

#include <otawa/proc/BBProcessor.h>
#include <otawa/icat3/features.h>
#include <otawa/hard/Memory.h>

#include <lrupreanalysis/features.h>

#include <lrupreanalysis/ExistHitDomain.h>
#include <lrupreanalysis/ExistMissDomain.h>

using namespace otawa;

namespace lrupreanalysis
{

namespace eh_em
{

class CacheState
{
public:
	CacheState(const otawa::icat3::LBlockCollection& coll, int set) :
			_existHitDomain(coll, set, nullptr),
			_existMissDomain(coll, set, nullptr),
			_existHitState(nullptr),
			_existMissState(nullptr)
	{
	}

	~CacheState()
	{
		if(_existHitState) {
			delete _existHitState;
		}
		if(_existMissState) {
			delete _existMissState;
		}
	}

	ExistHitDomain _existHitDomain;
	ExistMissDomain _existMissDomain;

	typename ExistHitDomain::t* _existHitState;
	typename ExistMissDomain::t* _existMissState;
};



ACSManager::ACSManager(WorkSpace* ws) :
	_underlyingManager(ws),
	_coll(**icat3::LBLOCKS(ws)),
	_b(nullptr)
{
	_states = new CacheState*[_coll.sets()];
	array::set(_states, _coll.sets(), null<CacheState>());
	_hasExistHit = ws->isProvided(EXIST_HIT_ANALYSIS_FEATURE);
	_hasExistMiss = ws->isProvided(EXIST_MISS_ANALYSIS_FEATURE);
}

ACSManager::~ACSManager()
{
	for(int i = 0; i < _coll.sets(); ++i) {
		if(_states[i] != nullptr) {
			delete _states[i];
		}
	}

	delete[] _states;
}

void ACSManager::start(otawa::Block* b)
{
	ASSERT(b);
	_b = b;
	_underlyingManager.start(b);
	_used.clear();
}

void ACSManager::update(const icache::Access& acc)
{
	icat3::LBlock* lb = icat3::LBLOCK(acc);
	CacheState& state = use(lb->set());
	if(_hasExistHit) {
		state._existHitDomain.update(acc, *state._existHitState, _underlyingManager);
	}

	if(_hasExistMiss) {
		state._existMissDomain.update(acc, *state._existMissState, _underlyingManager);
	}
	_underlyingManager.update(acc);
}

int ACSManager::mustAge(const icat3::LBlock* lb)
{
	return _underlyingManager.mustAge(lb);
}

int ACSManager::mayAge(const icat3::LBlock* lb)
{
	return _underlyingManager.mayAge(lb);
}

int ACSManager::existHitAge(const icat3::LBlock* lb)
{
	if(!_hasExistHit) {
		return 0; // FIXME
	}
	return use(lb->set())._existHitState->get(lb->index());
}

int ACSManager::existMissAge(const icat3::LBlock* lb)
{
	if(!_hasExistMiss) {
		return 0;
	}
	return use(lb->set())._existMissState->get(lb->index());
}

ExistHitDomain& ACSManager::ehDomain(int set)
{
	return use(set)._existHitDomain;
}

ExistHitDomain::t* ACSManager::ehValue(int set)
{
	return use(set)._existHitState;
}

ExistMissDomain& ACSManager::emDomain(int set)
{
	return use(set)._existMissDomain;
}

ExistMissDomain::t* ACSManager::emValue(int set)
{
	return use(set)._existMissState;
}

void ACSManager::print(const icat3::LBlock* lb, Output& out)
{
	CacheState& state = use(lb->set());
	if(_hasExistHit) {
		out << "{EH: ";
		state._existHitDomain.print(*state._existHitState, out);
	}
	if(_hasExistMiss) {
		out << ", EM";
		state._existMissDomain.print(*state._existMissState, out);
	}
	out << "}";
}

CacheState& ACSManager::use(int set)
{
	if(!_used.contains(set)) {
		if(_states[set] == nullptr) {
			_states[set] = new CacheState(_coll, set);
		}

		if(_states[set]->_existHitState == nullptr) {
			_states[set]->_existHitState = new ExistHitDomain::t;
		}
		_states[set]->_existHitDomain.copy(*_states[set]->_existHitState, (*EXIST_HIT_IN(_b))[set]);

	       	if(_states[set]->_existMissState == nullptr) {
       			_states[set]->_existMissState = new ExistMissDomain::t;
		}
	       	_states[set]->_existMissDomain.copy(*_states[set]->_existMissState, (*EXIST_MISS_IN(_b))[set]);
		_used.push(set);
	}

	return *_states[set];
}

class DUCatBuilder: public BBProcessor
{
public:
	static p::declare reg;
	DUCatBuilder(void): BBProcessor(reg), _ways(0), _man(nullptr)
	{
	}

protected:

	virtual void setup(WorkSpace *ws) override
	{
		const otawa::icat3::LBlockCollection* coll = icat3::LBLOCKS(ws);
		ASSERT(coll);
		_ways = coll->A();
		_man = new eh_em::ACSManager(ws);
	}

	virtual void cleanup(WorkSpace*) override
	{
		delete _man;
	}

	virtual void processBB(WorkSpace*, CFG*, Block *v) override
	{
		for(Block::EdgeIter e = v->outs(); e(); e++) {
			if(logFor(LOG_BLOCK)) {
				log << "\t\t\t\tprocess " << *e << io::endl;
			}

			if(v->isSynth() && v->toSynth()->callee()) {
				_man->start(v->toSynth()->callee()->exit());
			}
			else {
				_man->start(v);
			}

			processAccesses(*icache::ACCESSES(v));
			processAccesses(*icache::ACCESSES(*e));
		}
	}

	void processAccesses(Bag<icache::Access>& accs)
	{
		for(int i = 0; i < accs.count(); i++) {
			otawa::icat3::LBlock *lb = icat3::LBLOCK(accs[i]);

			int existHitAge = _man->existHitAge(lb);
			int existMissAge = _man->existMissAge(lb);

			bool eh = (existHitAge != _ways);
			bool em = (existMissAge == _ways);

			DUCategory cat;
			if(eh && em) {
				cat = DUCategory::DU;
			}
			else if(eh) {
				cat = DUCategory::EH;
			}
			else if(em) {
				cat = DUCategory::EM;
			}
			else {
				cat = DUCategory::NC;
			}

			if(logFor(LOG_BLOCK)) {
				log << "\t\t\t\t\tAccess " << accs[i] << " is ";
				switch(cat) {
					case DUCategory::DU:
						log << "DU";
					break;
					case DUCategory::EH:
						log << "EH";
					break;
					case DUCategory::EM:
						log << "EM";
					break;
					case DUCategory::NC:
						log << "NC";
					break;
				}
				log << io::endl;
			}

			DU_CATEGORY(accs[i]) = cat;
			_man->update(accs[i]);
		}
	}

	int _ways;
	eh_em::ACSManager* _man;
};

p::declare DUCatBuilder::reg = p::init("lrupreanalysis::eh_em::DUCatBuilder", Version(1, 0, 0))
	.extend<BBProcessor>()
	.make<DUCatBuilder>()
	.require(otawa::icat3::LBLOCKS_FEATURE)
	.require(lrupreanalysis::eh_em::EXIST_HIT_ANALYSIS_FEATURE)
	.require(lrupreanalysis::eh_em::EXIST_MISS_ANALYSIS_FEATURE)
	.provide(lrupreanalysis::eh_em::DU_CATEGORY_FEATURE);


/**
 * This feature ensures that definitely unknown category information is stored on each
 * @ref icache::Access found in the blocks and in the CFG of the current workspace.
 * Such information is made of @ref DUCategory specifying the behaviour of the cache
 * (EH -- Exist Hit, EM -- Exist Miss, DU -- Definitely Unknown (Exist Hit and Exist
 * Miss), NC -- Not-classified).
 *
 * @par Properties
 * @li @ref DU_CATEGORY
 *
 * @par Default implementation
 * @li @ref DUCatBuilder
 *
 * @ingroup lrupreanalysis
 */
p::feature DU_CATEGORY_FEATURE("lrupreanalysis::eh_em::DU_CATEGORY_FEATURE", DUCatBuilder::reg);


/**
 * Defines the instruction cache Definitely Unknown category for the @ref icache::Access where
 * this property is set.
 *
 * @par Hooks
 * @li @ref icache::Access
 *
 * @par Features
 * @li @ref DU_CATEGORY_FEATURE
 *
 * @ingroup lrupreanalysis
 */
p::id<DUCategory> DU_CATEGORY("lrupreanalysis::eh_em::DU_CATEGORY", DUCategory::NC);

} // namespace eh_em

} // namespace lrupreanalysis

#endif // LRUPREANALYSIS_DU_CAT_BUILDER_H_

